AmazonStorageBundle
===================

About
-----

This bundle provides easy amazonS3 and cloud front manipulation support for Symfony2.

## Installation

To install this bundle, you'll need "liip/imagine-bundle:~0.18.0", "knplabs/gaufrette:~0.1.6", "knplabs/knp-gaufrette-bundle:~0.1.6"
and "aws/aws-sdk-php:2.*".

### Step 1: Download AmazonStorageBundle using composer

Tell composer to require AmazonStorageBundle by running the command:

``` bash
$ php composer.phar require "vgrdominik/amazonstorage-bundle:dev-master"
```

### Step 2: Enable the bundles

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...

        new Liip\ImagineBundle\LiipImagineBundle(),
        new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
        new Amazon\StorageBundle\AmazonStorageBundle()
    );
}
```

### Step 3: Register the bundle's routes

Finally, add the following to your routing file:

``` yaml
# app/config/routing.yml

_imagine:
    resource: .
    type:     imagine
```

## Configuration
### AmazonS3 parameters:

``` yaml
# app/config/parameters.yml
amazon_aws_key: ''
amazon_aws_secret_key: ''
amazon_s3_bucket_name: ''
amazon.cdn.url: ''
```

### Thumbs:

``` yaml
liip_imagine:
    data_loader: amazon_storage.liip_imagine.custom_loader
    cache: amazon_storage.liip_imagine.custom_cache
    controller_action:    amazon_storage.liip_imagine.controller:filterAction
    filter_sets:
        thumb.200x200:
            quality: 50
            filters:
                thumbnail: { size: [200, 200], mode: inbound }
        thumb.200x200.local:
            cache:                web_path
            data_loader:          filesystem
            quality: 50
            filters:
                thumbnail: { size: [200, 200], mode: inbound }
```

### Assets function to generate the cdn url

``` yaml
framework:
    templating:
        engines: ['twig']
        packages:
            cdn:
                base_urls:
                    http: [%amazon.cdn.url%]
```

Upload files with PhotoUploader service
---------------------------------------

Implement Amazon\StorageBundle\Model\AmazonS3FileInterface in an entity or extend with Amazon\StorageBundle\Model\AmazonS3File.
The methods used are very simple.

To upload a file you only inject the photouploader service, update the entity and call upload method.

``` php
if (!empty($_FILES)) {
    try {
        $file = $request->files->get('test')['path'];
        $ext = $file->guessExtension();

        $path = 'image-test.'.$ext;
        $entity->setPath($path);
        $entity->setFile($file);
        $em->persist($entity);
        $em->flush();

        $uploader = $this->get('amazon_storage.storage.photo_uploader');
        if (!$uploader->upload($entity)) {
            return new Response("Error!");
        }
    } catch (\Exception $ex) {
        return new Response("Error!");
    }
}
```

Upload and view thumbs
----------------------

Only call image_file function and if the image exists in AmazonS3 the bundle will create the thumb. If not exists the function work normal (With local filesystem and cache).

``` twig
{{ asset(entity.getWebPath()|imagine_filter("thumb.200x200")) }}
```