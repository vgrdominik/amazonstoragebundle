<?php

namespace Amazon\StorageBundle\Service;

use Amazon\StorageBundle\Model\AmazonS3FileInterface;
use Gaufrette\Filesystem;

class PhotoUploader
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param AmazonS3FileInterface $file
     * @return string
     * @throws \Exception
     */
    public function upload(AmazonS3FileInterface $file)
    {
        $adapter = $this->filesystem->getAdapter();
        $adapter->setMetadata($file->getWebPath(), array('contentType' => $file->getFile()->getClientMimeType()));
        try{
            $adapter->write($file->getWebPath(), file_get_contents($file->getFile()->getPathname()));
        }catch(\Exception $e)
        {
            throw new \Exception('Image not uploaded. '.$e->getMessage());
        }


        return $file->getPath();
    }

    /**
     * @param $content
     * @param $path
     * @param $mimeType
     * @return string
     */
    public function uploadToCache($content, $path, $mimeType)
    {
        $adapter = $this->filesystem->getAdapter();
        $adapter->setMetadata($path, array('contentType' => $mimeType));
        $adapter->write($path, $content);

        return $path;
    }

}