<?php
namespace Amazon\StorageBundle\Service\LiipImagine;

use Liip\ImagineBundle\Imagine\Data\Loader\LoaderInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\ImageInterface;

class DataLoader implements LoaderInterface
{

    /**
     * @var string
     */
    private $basePath;
    /**
     * @var ImagineInterface
     */
    private $imagine;

    /**
     * @param $basePath
     * @param ImagineInterface $imagine
     */
    function __construct($basePath, ImagineInterface $imagine)
    {
        $this->setBasePath($basePath);
        $this->setImagine($imagine);
    }

    /**
     * @return string
     */
    private function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @param $basePath
     * @return string
     */
    private function setBasePath($basePath)
    {
        return $this->basePath = $basePath;
    }

    /**
     * @return ImagineInterface
     */
    private function getImagine()
    {
        return $this->imagine;
    }

    /**
     * @param $imagine
     * @return ImagineInterface
     */
    private function setImagine($imagine)
    {
        return $this->imagine = $imagine;
    }

    /**
     * @param mixed $path
     * @return bool|ImageInterface
     */
    function find($path)
    {
        $resource = @fopen($this->getBasePath().'/'.$path, 'r');

        if(!is_bool($resource))
        {
            return $this->getImagine()->open($this->getBasePath().'/'.$path);
        }else{
            return false;
        }
    }
}