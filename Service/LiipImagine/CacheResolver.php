<?php
namespace Amazon\StorageBundle\Service\LiipImagine;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Liip\ImagineBundle\Imagine\Cache\Resolver\AbstractFilesystemResolver;
use Gaufrette\Filesystem;
use Amazon\StorageBundle\Service\PhotoUploader;

class CacheResolver extends AbstractFilesystemResolver
{
    /**
     * @var PhotoUploader
     */
    private $uploader;
    /**
     * @var
     */
    private $storage;
    /**
     * @var
     */
    private $cdnUrl;

    /**
     * @param \Symfony\Component\Filesystem\Filesystem $uploader
     * @param Filesystem $storage
     * @param $cdnUrl
     */
    function __construct(PhotoUploader $uploader, Filesystem $storage, $cdnUrl)
    {
        $this->setUploader($uploader);
        $this->setStorage($storage);
        $this->setCdnUrl($cdnUrl);
    }

    /**
     *
     * @return PhotoUploader
     */
    private function getUploader()
    {
        return $this->uploader;
    }

    /**
     * @param PhotoUploader $uploader
     * @return PhotoUploader
     */
    private function setUploader(PhotoUploader $uploader)
    {
        return $this->uploader = $uploader;
    }

    /**
     * @return string
     */
    private function getCdnUrl()
    {
        return $this->cdnUrl;
    }

    /**
     * @param $cdnUrl
     * @return string
     */
    private function setCdnUrl($cdnUrl)
    {
        return $this->cdnUrl = $cdnUrl;
    }

    /**
     *
     * @return Filesystem
     */
    private function getStorage()
    {
        return $this->storage;
    }

    /**
     * @param $storage
     * @return Filesystem
     */
    private function setStorage($storage)
    {
        return $this->storage = $storage;
    }

    /**
     * {@inheritDoc}
     */
    protected function getFilePath($path, $filter)
    {
        $browserPath = $this->decodeBrowserPath($this->getBrowserPath($path, $filter));

        if (!empty($this->basePath) && 0 === strpos($browserPath, $this->basePath)) {
            $browserPath = substr($browserPath, strlen($this->basePath));
        }

        return $this->cacheManager->getWebRoot().$browserPath;
    }

    /**
     * Decodes the URL encoded browser path.
     *
     * @param string $browserPath
     *
     * @return string
     */
    protected function decodeBrowserPath($browserPath)
    {
        //TODO: find out why I need double urldecode to get a valid path
        return urldecode(urldecode($browserPath));
    }

    /**
     * @param Request $request
     * @param string $path
     * @param string $filter
     * @return string|RedirectResponse
     */
    public function resolve(Request $request, $path, $filter)
    {
        $objectPath = $this->getObjectPath($path, $filter);

        if ($this->objectExists($objectPath)) {
            return new RedirectResponse($this->getObjectUrl($objectPath), 301);
        }

        return $objectPath;
    }

    /**
     * @param string $path
     * @param string $filter
     * @param bool $absolute
     * @return string
     */
    public function getBrowserPath($path, $filter, $absolute = false)
    {
        $objectPath = $this->getObjectPath($path, $filter);
        if ($this->objectExists($objectPath)) {
            return $this->getObjectUrl($objectPath);
        }

        return $this->cacheManager->generateUrl($path, $filter, $absolute);
    }

    /**
     * @param $path
     * @param $filter
     * @return string
     */
    protected function getObjectPath($path, $filter)
    {
        return str_replace('//', '/', $filter.'/'.$path);
    }

    /**
     * Returns the URL for an object saved on Amazon S3.
     *
     * @param string $targetPath
     *
     * @return string
     */
    protected function getObjectUrl($targetPath)
    {
        return $this->getCdnUrl().'/'.$targetPath;
    }

    /**
     * Checks whether an object exists.
     *
     * @param string $objectPath
     *
     * @return bool
     */
    protected function objectExists($objectPath)
    {
        return $this->getStorage()->has($objectPath);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($cachePrefix)
    {
        // La cache de amazon S3 se elimina manualmente
    }

    /**
     * @param string $targetPath
     * @param string $filter
     * @return bool
     */
    public function remove($targetPath, $filter)
    {
        // Los thumbs de amazon S3 se eliminan manualmente
        return true;
    }

    /**
     * @param Response $response
     * @param string $path
     * @param string $filter
     * @return Response
     */
    public function store(Response $response, $path, $filter)
    {
        $this->getUploader()->uploadToCache($response->getContent(), $filter.'/'.$path, $response->headers->get('Content-Type'));
        $response->setStatusCode(201);

        return $response;
    }
}