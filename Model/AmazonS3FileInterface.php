<?php

namespace Amazon\StorageBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface AmazonS3FileInterface
{

    /**
     * @return string
     */
    public function getPath();

    /**
     * @param $path
     */
    public function setPath($path);

    /**
     * @return UploadedFile
     */
    public function getFile();

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file);

    /**
     * @return string
     */
    public function getWebPath();

    /**
     * @return string
     */
    public function getUploadDir();

}
